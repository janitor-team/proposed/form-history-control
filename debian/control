Source: form-history-control
Section: web
Priority: optional
Standards-Version: 4.4.1
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@lists.alioth.debian.org>
Uploaders: Dmitry Smirnov <onlyjob@debian.org>
Build-Depends: debhelper-compat (= 12)
    ,jq
    ,libjs-jquery
    ,libjs-jquery-datatables
    ,libjs-jquery-mousewheel
Homepage: https://stephanmahieu.github.io/fhc-home/
Vcs-Browser: https://salsa.debian.org/webext-team/form-history-control
Vcs-Git: https://salsa.debian.org/webext-team/form-history-control.git

Package: webext-form-history-control
Provides: firefox-form-history-control
Architecture: all
Built-Using: ${my:Built-Using}
Depends: ${misc:Depends}
Recommends: ${misc:Recommends}
    ,firefox-esr (>= 60) | firefox (>= 60)
Enhances: firefox
Breaks: xul-ext-form-history-control
Replaces: xul-ext-form-history-control
Description: extension to manage form history
 An extension to View and Manage all form data that has been saved by the
 web browser giving you full control over what is stored, what is cleaned up
 or not, and when to perform a cleanup. This extension even allows you to
 control selectively for which webpages form history data is stored (either
 blacklist or whitelist). Also stores text from editor fields as you type
 for easy recovery in case of disaster.
 .
 This extension enables you to selectively delete privacy sensitive
 information without having to delete the entire history. With the help of
 powerful regular expressions, information can simply and effectively be
 retrieved, edited or deleted. Use it to correct misspelled entries, get rid
 of sensitive data like private banking info or delete passwords that were
 entered into the wrong formfield.
 .
 This plugin also stores text from editor fields as you type, so you never
 have to loose your work when disaster strikes. Recover your lost work after
 session timeouts, network failures, browser crashes, power failures and all
 other things that will destroy the hard work you just put into writing that
 important email, essay or blog post.
 .
 Form History Control can filter data either by keyword, active page, active
 field or cleanup criteria. Powerful advanced search options allows you to
 find information by name, value, timeframe, usage or host. Optional regular
 expressions provide a very effective way to retrieve any information you
 might look for. The advanced search option comes with a list of predefined
 regular expressions which can be altered or extended to fit any need.
 .
 The extension also offers the possibility to export/import form history
 data and configuration settings, allowing you to exchange data between
 multiple browser configurations or import history data into your own
 application of choice.
 .
 Form History Control can also be used to easily autofill textfields in a
 web form using either the most used or the last used formhistory data.
